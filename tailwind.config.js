/** @type {import('tailwindcss').Config} */
export default {
  darkMode: 'class',
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    extend: {
      fontSize: {
        icon: ['9px', 1],
        iconsmall: ['6px', 1],
        noti: ['26px', 1],
      },
      colors: {
        gray: {
          50: '#f9fafb',
          100: '#f3f4f6',
          200: '#e5e7eb',
          300: '#d1d5db',
          400: '#969798',
          500: '#666666',
          600: '#4b5563',
          700: '#353535',
          800: '#262626',
          900: '#171818',
        },
        primary: {
          50: '#fff7ed',
          100: '#ffedd5',
          200: '#fed7aa',
          300: '#fdba74',
          400: '#fb923c',
          500: '#f97316',
          600: '#ea580c',
          700: '#c2410c',
          800: '#9a3412',
          900: '#7c2d12',
          950: '#431407',
        },
        success: {
          normal: '#84cc16',
        },
        error: {
          normal: '#ef4444',
        },
        warning: {
          normal: '#FF9800',
        },
        info: {
          normal: '#03A9F4',
        },
      },
    },
  },
  plugins: [],
};
