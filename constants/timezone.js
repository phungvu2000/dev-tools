import moment from 'moment-timezone';
const timezone = moment.tz.names();

export const timezoneOptions = moment.tz
  .names()
  .map((name) => {
    const now = Date.now();
    const zone = moment.tz.zone(name);
    return { name, offset: zone !== null ? zone.utcOffset(now) : 0 };
  })
  .sort((a, b) =>
    a.offset === b.offset ? a.name.localeCompare(b.name) : b.offset - a.offset
  )
  .map((zone) => {
    const gmtOffset = `GMT${moment.tz(zone.name).format('Z')}`;
    // const abbr = moment.tz(zone.name).format('z');
    return {
      value: zone.name,
      label: `(${gmtOffset}) ${zone.name}`,
    };
  });

export const currentTimezone = moment.tz.guess();
