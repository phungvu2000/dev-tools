export const notificationList = ref([]);

function success({ title, description, duration }) {
  let notificationInfo = {
    id: new Date().getTime() + Math.random(),
    type: 'success',
    title: title,
    description: description,
  };

  notificationList.value.push(notificationInfo);
  if (duration) {
    setTimeout(function () {
      notificationList.value = notificationList.value.filter(
        (item) => item.id != notificationInfo.id
      );
    }, duration);
  }
}

function info({ title, description, duration }) {
  let notificationInfo = {
    id: new Date().getTime() + Math.random(),
    type: 'info',
    title: title,
    description: description,
  };

  notificationList.value.push(notificationInfo);
  if (duration) {
    setTimeout(function () {
      notificationList.value = notificationList.value.filter(
        (item) => item.id != notificationInfo.id
      );
    }, duration);
  }
}

function warning({ title, description, duration }) {
  let notificationInfo = {
    id: new Date().getTime() + Math.random(),
    type: 'warning',
    title: title,
    description: description,
  };

  notificationList.value.push(notificationInfo);
  if (duration) {
    setTimeout(function () {
      notificationList.value = notificationList.value.filter(
        (item) => item.id != notificationInfo.id
      );
    }, duration);
  }
}

function error({ title, description, duration }) {
  let notificationInfo = {
    id: new Date().getTime() + Math.random(),
    type: 'error',
    title: title,
    description: description,
  };

  notificationList.value.push(notificationInfo);
  if (duration) {
    setTimeout(function () {
      notificationList.value = notificationList.value.filter(
        (item) => item.id != notificationInfo.id
      );
    }, duration);
  }
}

function close(id) {
  notificationList.value = notificationList.value.filter(
    (item) => item.id != id
  );
}

export const notification = {
  success,
  info,
  warning,
  error,
  close,
};
