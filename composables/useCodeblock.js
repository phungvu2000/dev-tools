export default function useCodeblock(emits) {
  // Reactive
  const extensions = ref([]);
  // Codemirror EditorView instance ref
  const view = shallowRef();
  const onReady = (payload) => {
    view.value = payload.view;
    emits('ready', view);
  };

  // Methods
  function onChange(event) {
    emits('update:value', event);
    emits('change', event);
  }
  function onFocus(event) {
    emits('focus', event);
  }
  function onBlur(event) {
    emits('blur', event);
  }

  return { extensions, onReady, onChange, onFocus, onBlur };
}
