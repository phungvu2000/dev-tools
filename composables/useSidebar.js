export default function useSidebar() {
  const categoryList = [
    {
      label: 'Home',
      icon: '<i class="pv pv-home"></i>',
      href: {
        name: 'index',
      },
    },
    {
      label: 'Converters',
      icon: '<i class="pv pv-left-right"></i>',
      isShowChildren: true,
      children: [
        {
          label: 'JSON <> YAML',
          href: {
            name: 'converter-jsonvsyaml',
          },
        },
        {
          label: 'Timestamp',
          href: {
            name: 'converter-timestamp',
          },
        },
        {
          label: 'Number Base',
          href: {
            name: 'converter-numberbase',
          },
        },
        {
          label: 'Cron parser',
          href: {
            name: 'converter-cron-parser',
          },
        },
      ],
    },
    {
      label: 'Encoders/Decoders',
      icon: '<i class="pv pv-binary-code"></i>',
      // href: {
      //   name: 'index',
      // },
      children: [
        {
          label: 'Certificate',
          href: {
            name: 'code-certificate',
          },
        },
        {
          label: 'HTML',
          href: {
            name: 'code-html-encode',
          },
        },
        {
          label: 'URL',
          href: {
            name: 'code-url',
          },
        },
        {
          label: 'Base64 Text',
          href: {
            name: 'code-base64-text',
          },
        },
        {
          label: 'Base64 Image',
          href: {
            name: 'code-base64-image',
          },
        },
        {
          label: 'Gzip',
          href: {
            name: 'code-gzip',
          },
        },
        {
          label: 'JWT',
          href: {
            name: 'code-jwt',
          },
        },
      ],
    },
    {
      label: 'Formatters',
      icon: '<i class="pv pv-left-align"></i>',
      // href: {
      //   name: 'index',
      // },
      children: [
        {
          label: 'JSON',
          href: {
            name: 'formatter-json',
          },
        },
        {
          label: 'SQL',
          href: {
            name: 'index',
          },
        },
        {
          label: 'XML',
          href: {
            name: 'index',
          },
        },
      ],
    },
    {
      label: 'Generators',
      icon: '<i class="pv pv-convert-file"></i>',
      // href: {
      //   name: 'index',
      // },
      children: [
        {
          label: 'Hash',
          href: {
            name: 'index',
          },
        },
        {
          label: 'UUID',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Lorem Ipsum',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Checksum',
          href: {
            name: 'index',
          },
        },
      ],
    },
    {
      label: 'Text',
      icon: '<i class="pv pv-text-format"></i>',
      // href: {
      //   name: 'index',
      // },
      children: [
        {
          label: 'Escape / Unescape',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Inpector & Case Converter',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Regex Tester',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Text Diff',
          href: {
            name: 'index',
          },
        },
        {
          label: 'XML Validator',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Markdown Preview',
          href: {
            name: 'index',
          },
        },
      ],
    },
    {
      label: 'Graphic',
      icon: '<i class="pv pv-picture"></i>',
      // href: {
      //   name: 'index',
      // },
      children: [
        {
          label: 'Color Blindness Simulator',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Color Picker & Constrast',
          href: {
            name: 'index',
          },
        },
        {
          label: 'PNG / JPEG Compressor',
          href: {
            name: 'index',
          },
        },
        {
          label: 'Image Converter',
          href: {
            name: 'index',
          },
        },
      ],
    },
  ];

  const configCategoryList = [
    {
      label: 'Setting',
      icon: '<i class="pv pv-cog"></i>',
      href: {
        name: 'index',
      },
    },
  ];

  const isShowSidebar = ref(false);

  function onToggleSidebar() {
    isShowSidebar.value = !isShowSidebar.value;
  }

  function onHiddenSidebar() {
    isShowSidebar.value = false;
  }

  const searchInput = ref('');
  const categoryFilted = computed(() => {
    return filterCategory(categoryList, searchInput.value);
  });

  function filterCategory(array, text) {
    let newArray = [];
    for (let item of array) {
      let { children, ...rest } = item;
      newArray.push(rest);
      rest.children = [];
      if (item.children) {
        for (let child of item.children) {
          if (child.label.toLowerCase().indexOf(text.toLowerCase()) > -1) {
            rest.children.push(child);
          }
        }
      }
    }
    return newArray.filter((item) => {
      return (
        (item?.children && item.children?.length > 0) ||
        item.label.toLowerCase().indexOf(text.toLowerCase()) > -1
      );
    });
  }

  return {
    searchInput,
    isShowSidebar,
    onToggleSidebar,
    onHiddenSidebar,
    categoryList,
    categoryFilted,
    configCategoryList,
  };
}
